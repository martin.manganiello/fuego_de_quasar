# Fuego de Quasar
La ubicacion de la nave es: x = -500, y = 250  
En base a esta ubicacion se obtiene la distancia que la separa de cada uno de los satellites.

# Desarrollo local en container con VS CODE
Requisitos:
- Docker
- Docker-compose
- VS Code

En la carpeta raiz correr:  
```docker-compose up -d```  
Ahora esta corriendo el container. Adjuntamos VS Code al container.
![Attaching VS Code to container](/assets/attaching_container_to_vscode.png)
Podemos empezar a trabajar:
![VS Code attached](/assets/vscode_attached.png)
Establecer workdir como /var/task
![Set workdir](/assets/set_workdir.png)

Instalar extensiones para VS Code (Python)
![Extensions](/assets/install_python_extension.png)

Set Python interpreter
![Set Python interpreter](/assets/python_interpreter.png)

# Corriendo tests
``` pytest tests/tests.py ```

# Levantar API
```flask run```

# Postman collection:
<a href="https://www.getpostman.com/collections/a39923591d1253119335" rel="some text">![Postman Collection](assets/postman_logo.png)</a>

# Topsecret
## Payload:
```
{
"satellites":[
{
"name":"kenobi",
"distance":450,
"message":["este","","","mensaje",""]
},
{
"name":"skywalker",
"distance":694.6221994724903,
"message":["","es","","","secreto"]
},
{
"name":"sato",
"distance":1011.1874208078342,
"message":["este","","un","",""]
}
]
}
```
## API Topsecret Response:
![topsecret](/assets/api_response_topsecret.png)
## Trilateration plot:
![Trilateration plot](/assets/trilateration.png)

## Payload del enunciado (404):
```
{
"satellites":[
{
"name":"kenobi",
"distance":100,
"message":["este","","","mensaje",""]
},
{
"name":"skywalker",
"distance":115.5,
"message":["","es","","","secreto"]
},
{
"name":"sato",
"distance":142.7,
"message":["este","","un","",""]
}
]
}
```
Como los circulos no se superponen no puede obtener la ubicacion.
## API Topsecret Response: (404)
![topsecret 404](/assets/api_response_topsecret_404.png)

# Despliegue en AWS usando Zappa:
Add .env file to root folder like this example:
AWS_ACCESS_KEY_ID=ABC123ABC123
AWS_SECRET_ACCESS_KEY=ABC123ABC123
FLASK_APP=lambda_function.py

```zappa init```

```zappa deploy prod```

# AWS:

https://7awa0eczze.execute-api.sa-east-1.amazonaws.com/prod
