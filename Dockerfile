FROM python:3.8

USER root
# Prevents Python from writing pyc files to disc
ENV PYTHONDONTWRITEBYTECODE 1
# Prevents Python from buffering stdout and stderr
ENV PYTHONUNBUFFERED 1
ENV AWS_DEFAULT_REGION=sa-east-1 \
    BUCKET=FAKE \
    KEY=FAKE \
    ENV=DEV \
    DEBUG=1 \
    FLASK_DEBUG=1 \
    SERVER_NAME=localhost \
    SERVER_PORT=5000 \
    HTTP_HOST=localhost:5000 \
    FLASK_RUN_FROM_CLI=1
ENV LAMBDA_TASK_ROOT /var/task
RUN pip install --upgrade pip
WORKDIR $LAMBDA_TASK_ROOT
COPY requirements.txt $LAMBDA_TASK_ROOT
RUN pip install -r requirements.txt
COPY . $LAMBDA_TASK_ROOT
