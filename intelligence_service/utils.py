import math
import json
from decimal import Decimal
import datetime
import boto3
from boto3.dynamodb.conditions import Key, Attr
from json.decoder import JSONDecodeError
from pygeodesy.vector3d import trilaterate3d2, Vector3d
from pygeodesy.errors import IntersectionError
from intelligence_service.validation import ValidationException, IntelligenceException

ACTIVE_SATELLITES = {
    "kenobi": Vector3d(-500, -200, 1),
    "skywalker": Vector3d(100, -100, 1),
    "sato": Vector3d(500, 100, 1)
}

dynamodbResource = boto3.resource('dynamodb')
table_name = 'satellites_info'
table = dynamodbResource.Table(table_name)

def get_satellites():
    now = datetime.datetime.now()
    timestamp = now.isoformat()
    deadline = str(now - datetime.timedelta(hours=0, minutes=30))
    response = table.scan(FilterExpression=Attr('timestamp').between(deadline, timestamp))
    satellites = list()
    for data in response['Items']:
        data['distance'] = float(data['distance'])
        satellites.append(data)
    return satellites


def save_satellite_data(data):
    name = data['name']
    distance = data['distance']
    message = data['message']
    timestamp = datetime.datetime.now().isoformat()
    item = {
        "name": name,
        "distance": Decimal(str(distance)),
        "message": message,
        "timestamp": timestamp,
    }
    response = table.put_item(Item=item)
    return True if response else False


def request_validation(request):
    """ Valid request """
    try:
        data = json.loads(request.decode('utf8'))
        if "satellites" not in data: raise ValueError()
        return data['satellites']
    except (ValueError, JSONDecodeError) as e:
        raise ValidationException("Data contains errors: " + str(e) + ' - '+ str(request))


def get_distance(p1, p2):
    """ Returns the distance between two cartesian points.
        args: (x1, y1), (x2, y2)
        returns: float
    """
    return math.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)


def get_message(satellites):
    """ Returns the decoded message
    input: the message as it is received on each satellite
    output: the message as generated by the sender of the message
    """
    messages = [satellite["message"] for satellite in satellites]
    decoded_message = ['' for _ in range(len(messages[0]))]
    for message in messages:
        for i, v in enumerate(message):
            if v != "" and decoded_message[i] == "":
                decoded_message[i] = v
            else:
                continue
    return ' '.join(decoded_message)


def get_location(satellites):
    """
    input: distance to the transmitter as it is received on each satellite
    output: the 'x' and 'y' coordinates of the sender of the message
    """
    for satellite in satellites:
        satellite.update({"point": ACTIVE_SATELLITES[satellite['name']]})
        satellite.update({"distance": satellite['distance']})
    if len(satellites) == 3:
        p1 = satellites[:3][0]['point']
        r1 =  satellites[:3][0]['distance']

        p2 = satellites[:3][1]['point']
        r2 =  satellites[:3][1]['distance']

        p3 = satellites[:3][2]['point']
        r3 =  satellites[:3][2]['distance']
        try:
            v1, _ = trilaterate3d2(p1, r1, p2, r2, p3, r3)
        except IntersectionError as e:
            raise IntelligenceException("Location cannot be determined")
    else:
        raise ValidationException("At least three satellites are needed")
    return v1
