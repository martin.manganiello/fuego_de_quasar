class ValidationException(Exception):
    pass

class IntelligenceException(Exception):
    pass
