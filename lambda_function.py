import os
import json
from flask import Flask, request, jsonify, render_template
from intelligence_service.validation import ValidationException, IntelligenceException
from intelligence_service.utils import (
    get_satellites,
    get_message,
    get_location,
    request_validation,
    save_satellite_data,
    ACTIVE_SATELLITES
)

app = Flask(__name__, template_folder='assets/')


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/topsecret', methods=['POST'])
def topsecret():
    try:
        satellites = request_validation(request.data)
        location_ship = get_location(satellites)
        message = get_message(satellites)
    except ValidationException as e:
        return jsonify({"error_message": str(e)}), 400
    except IntelligenceException as e:
        return jsonify({"error_message": str(e)}), 404
    payload = {
        "position": {
            "x": location_ship.x,
            "y": location_ship.y,
        "message": message
    }}
    return jsonify(payload), 200


@app.route('/topsecret_split/', methods=['GET'])
@app.route('/topsecret_split/<satellite_name>', methods=['POST'])
def topsecret_split(satellite_name=None):
    if request.method == 'POST' and satellite_name:
        satellite_name = satellite_name.lower()
        if satellite_name not in ACTIVE_SATELLITES.keys():
            return jsonify({"error_message": "{} isn't an active satellite".format(satellite_name)}), 404
        data = json.loads(request.data)
        data.update({"name": satellite_name})
        saved_data = save_satellite_data(data)
        if saved_data:
            return jsonify({"error_message": "{satellite_name} updated data".format(satellite_name=satellite_name)}), 200
        else:
            return jsonify({"error_message": "Error updating DB"}), 500
    else:
        try:
            satellites = get_satellites()
            location = get_location(satellites)
            message = get_message(satellites)
        except ValidationException as e:
            return jsonify({"error_message": str(e)}), 400
        except IntelligenceException as e:
            return jsonify({"error_message": str(e)}), 404
        payload = {
            "position": {
                "x": location.x,
                "y": location.y,
            "message": message
        }}
        return jsonify(payload), 200


if __name__ == '__main__':
    env = os.environ['ENV']
    debug = True if env == 'DEV' else False
    app.run(debug=debug)
