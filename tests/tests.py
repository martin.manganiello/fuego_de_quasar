import pytest
from lambda_function import app
from intelligence_service.utils import get_distance, get_message, get_location

"""
    Satelites activos (x, y):
    Kenobi: -500, -200
    Skywalker: 100, -100
    Sato: 500, 100
"""
# Los circulos no se superponen y no se puede obtener la ubicacion.
input_location_unknown = [
    {
        "name": "kenobi",
        "distance": 100.0,
        "message": ["este", "", "", "mensaje", ""]
    },
    {
        "name": "skywalker",
        "distance": 115.5,
        "message": ["", "es", "", "", "secreto"]
    },
    {
        "name": "sato",
        "distance": 142.7,
        "message": ["este", "", "un", "", ""]
    }]
expected_location_unknown = None

# Los circulos se superponen y se puede obtener la ubicacion.
input_location_found = [
    {
        "name":"kenobi",
        "distance":450,
        "message":["este","","","mensaje",""]
    },
    {
        "name":"skywalker",
        "distance":694.6221994724903,
        "message":["","es","","","secreto"]
    },
    {
        "name":"sato",
        "distance":1011.1874208078342,
        "message":["este","","un","",""]
    }]

expected_location_found = {
    "position": {
        "x": "-499.99999999999636",
        "y": "249.9999999999575",
    "message": "este es un mensaje secreto"
}}
location_found = (input_location_found, expected_location_found)

@pytest.fixture
def fixture():
    return [
        {
            "name": "kenobi",
            "distance": 100.0,
            "message": ["este", "", "", "mensaje", ""]
        },
            {
            "name": "skywalker",
            "distance": 115.5,
            "message": ["", "es", "", "", "secreto"]
        },
        {
            "name": "sato",
            "distance": 142.7,
            "message": ["este", "", "un", "", ""]
        }
    ]


@pytest.mark.parametrize("test_input, expected", [
    location_found,
    pytest.param(input_location_unknown, expected_location_unknown, marks=pytest.mark.xfail(reason='Location cannot be determined')),
])
def test_get_location(test_input, expected):
    satellites, expected = test_input, expected
    location_ship = get_location(satellites)
    if location_ship:
        assert str(location_ship.x) == expected['position']['x']
        assert str(location_ship.y) == expected['position']['y']


def test_get_message(fixture):
    satellites = fixture
    message = get_message(satellites)
    assert message == 'este es un mensaje secreto'
